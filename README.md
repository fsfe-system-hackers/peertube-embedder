# Peertube Embedder

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/peertube-embedder/00_README)

A helper script to fetch videos from the FSFE's Peertube instance in all
available resolutions and store them with a well-defined name. It also takes
care of converting the MP4 originals into WEBM once a day, run as a cron job.

## Usage

The tool is executed on the download server, or any other place where you would
like to store the mirrored videos. Let's assume, the script as well as the video
storage location is `/srv/download/videos/peertube/`.

The server needs to have the following packages installed: `curl`, `jq`, and
[`yt-dlp`](https://github.com/yt-dlp/yt-dlp#installation)

### First-time addition of a video

On the server, log in to the server (in this example, the [FSFE's download
server](https://wiki.fsfe.org/TechDocs/DownloadServer)) navigate to wherever you
want to store the videos (which is also where this script has to be located),
run the necessary commands:

1. Log in to the server: `ssh download@download.fsfe.org -p 10222`
2. Navigate to the directory: `cd /srv/download/videos/peertube/`
3. Download the desired video, e.g.:
   `./peertube-embed.sh https://media.fsfe.org/w/xs29yhLxSP1uKLYkSeoKKp`
4. Check whether the download went alright. Copy the snippet and add it to the
   website.

The HTML/XML code `<peertube
url="https://media.fsfe.org/w/xs29yhLxSP1uKLYkSeoKKp" />` expands on the website
as a HTML5 video with all defined resolutions and each in MP4 and WEBM format.
You should wrap it in a `<figure>` just [as we do it with
images](https://fsfe.org/contribute/web/features.html#graphics).

### Conversion to WEBM

WEBM is a better format than MP4, although not as widely supported. However, due
to technical reasons, Peertube stores all videos in MP4, and the conversion to
WEBM takes a lot of time. Therefore, the conversion is not done live during your
first-time download, but once a day during the night automatically by a cron job
[^1].

This means that the snippet you copied will reference non-existing WEBM videos
until the next run. This should not be a problem as there is a fall-back with
the MP4 videos in place.

## Caveats

The solution presented here has a few drawbacks. Ideally, the whole conversion
would happen within Peertube and not require a detour via the download server.

* No subtitles: You could manually add subtitle tracks present in Peertube but
  that's quite complex and prone to errors.
* Views don't count: Video views via the website would not add to the statistics
  of Peertube.
* No flexible resolution setting: On Peertube, you can change the resolution
  live, and it has an auto-detection. That's not possible in this solution, and
  most browser will always play the 1080p video (highest resolution), even if
  their screens are smaller.

Therefore, we suggest to link to the video on Peertube below the video, e.g. in
the `<figcaption>`.



[^1]: `30 2 * * * chronic /srv/download/videos/peertube/peertube-embed.sh webm-convert`
