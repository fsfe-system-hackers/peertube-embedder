#!/usr/bin/env bash

set -euo pipefail

# This script has two modes
# 1. Default: ./peertube-embed.sh https://media.fsfe.org/w/foobar
#    Downloads a specific video in different resolutions in MP4 format and
#    outputs a snippet to include on fsfe.org
#
# 2. WEBM conversion: ./peertube-embed.sh webm-convert
#    For all MP4 videos in the current directory, convert them to WEBM.
#    In order to do this, it calls yt-dlp to download the MP4 video (which is
#    already there) calling the WEBM recoding.

# Resolutions we want to support
RESOLUTIONS="1080p 720p 360p"

# Domain of Peertube instance
PEERTUBE_DOMAIN="https://media.fsfe.org"

# Get available resolutions
function get_resolutions() {
  url=$1
  resolutions=$(yt-dlp "$url" --list-formats)
  grep -Eo "^[0-9]*?p" <<< "$resolutions"
}

# Peertube URL, e.g. https://media.fsfe.org/w/ivu4RA68z3BwbxG1wb5gdx
URL=$1

# Enter directory of script
cd "$(/usr/bin/dirname "$0")" || exit 1

if [[ -z $URL ]]; then
  echo "ERROR: you have to provide a valid Peertube URL, or run it with the \"webm-convert\" parameter"
  exit 1
fi

# First one-time download of MP4 videos with defined URL
if [[ "$URL" != "webm-convert" ]]; then

  if ! grep -q $PEERTUBE_DOMAIN <<< $URL; then
    echo "ERROR: the URL does not seem to be a valid video from $PEERTUBE_DOMAIN"
    exit 1
  fi

  # Remove potential trailing slash
  url=$(sed -E 's|/$||' <<< "$URL")

  # Get short uuid
  id=${url##*/}

  # Download poster
  poster=${PEERTUBE_DOMAIN}$(curl "${PEERTUBE_DOMAIN}/api/v1/videos/${id}" | jq -r ".previewPath" )
  curl -o "${id}".jpg "$poster"

  # Download in defined resolutions
  RESOLUTIONS_AVAILABLE=$(get_resolutions "$url")
  for res in $RESOLUTIONS; do
    # Only download if resolution actually available
    if [[ $RESOLUTIONS_AVAILABLE == *"$res"* ]]; then
      yt-dlp -f "$res" "$url" -o "${id}"_"${res}".mp4
    else
      echo "INFO: Video $url not available in $res, skipping..."
    fi
  done

  echo
  echo "All MP4 files have been downloaded. The WEBM files are converted in the background and be available within the next minutes or hours, depending on size and quality."
  echo
  echo "Please use the following snippet to include it on the website:"
  echo
  echo "<figure>"
  echo "  <peertube url=\"$url\" />"
  echo "</figure>"


# Conversion of all available videos in MP4 format to WEBM
else

  # Get all locally available video IDs
  files=
  for file in *.mp4; do
    files="$file
$files"
  done

  # remove suffixes
  files=$(echo "$files" | sed -E 's/_[0-9]*p\.mp4//g')
  # sort and unique
  ids=$(sort -u <<< $files)

  # Recode video in webm for all defined resolutions
  for id in $ids; do
    url="${PEERTUBE_DOMAIN}/w/${id}"
    RESOLUTIONS_AVAILABLE=$(get_resolutions "$url")
    for res in $RESOLUTIONS; do
      if [[ $RESOLUTIONS_AVAILABLE == *"$res"* ]]; then
        # "--recode-video webm -k" converts to webm (vp9/opus) and keeps original
        # mp4 file downloaded earlier
        yt-dlp --recode-video webm -k -f "$res" "$url" -o "${id}"_"${res}".mp4
      else
        echo "INFO: Video $url not available in $res, skipping..."
      fi
    done
  done

fi
